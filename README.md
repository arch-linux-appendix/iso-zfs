A daily build repository of Arch Linux + ZFS ISOs. This is
meant to ease the transition problems caused by license
incompatibility.

[Release Page](https://kayg.org/public/archlinux/iso/minimal/)

**Additional Setup**

The repository `archzfs` needs key verification before
packages in it can be installed. There are two ways to solve this:

-   Insert `SigLevel = TrustAll` before the `Server` block of
    `archzfs` repository in `/etc/pacman.conf`.

OR (the recommended way is)

-   `pacman-key --init`
-   `pacman-key --recv-keys F75D9D76`
-   `pacman-key --lsign-key F75D9D76`

